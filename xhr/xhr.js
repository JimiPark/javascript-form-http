const getBtn = document.getElementById('get-btn');
const postBtn = document.getElementById('post-btn');

const sendHttpRequest = (method, url, data) =>{
    const promise = new Promise((resolve, reject)=>{
        const xhr = new XMLHttpRequest();
        xhr.open(method, url);

        xhr.responseType = 'json';

        if(data){
            xhr.setRequestHeader('Content-Type', 'application/json');
        }

        xhr.onload = () => {
            if(xhr.status >=400){
                reject(xhr.response); 
            } else{
                resolve(xhr.response);
            }
        }

        xhr.onerror = () => {
            reject('Something Wrong');
        }
        xhr.send(JSON.stringify(data))
        // xhr.send(JSON.stringify(data));
    });

    return promise;
}

const getData = () => {
    sendHttpRequest('GET', 'https://reqres.in/api/users').then(responseData=>{
        console.log(responseData);
    });
    // const xhr = new XMLHttpRequest();
    // xhr.open('GET', 'https://reqres.in/api/users');

    // xhr.onload = () => {
    //     const data = JSON.parse(xhr.response);
    //     console.log(xhr.response);
    // };

    // xhr.send();
};

const sendData = () => {
    sendHttpRequest('POST', 'https://reqres.in/api/register', {
        email: 'eve.holt@reqres.in',
        // password: 'pistol'
    }).then(responseData => {
        console.log(responseData);
    }).catch(err=>{
        console.log(err);
    })

    // const xhr = new XMLHttpRequest();
    // xhr.open('POST', 'https://reqres.in/api/register');

    // xhr.responseType = 'json';

    // xhr.setRequestHeader('Content-Type', 'application/json');

    // xhr.onload = () => {
    //     // const data = JSON.parse(xhr.response);
    //     console.log(xhr.response);
    // };

    // xhr.onerror = () => {
    //     console.log('here is error');
    // }

    // const data = {
    //     email: 'eve.holt@reqres.in',
    //     password: 'pistol'
    // }

    // xhr.send(JSON.stringify(data));
};

getBtn.addEventListener('click', getData);
postBtn.addEventListener('click', sendData);