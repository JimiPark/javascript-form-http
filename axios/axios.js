const getBtn = document.getElementById('get-btn');
const postBtn = document.getElementById('post-btn');
const axiosbtn = document.getElementById('axiosbtn');

const getData = () => {
    axios.get('https://reqres.in/api/users').then((result)=>{
        console.log(result);
    })
}

const postData = () => {

    // const testform = document.getElementById('myForm');
    // formData = new FormData(testform);
    // formData.append('email', this.email);
    // formData.append('password', this.password);
    // console.log(formData.get('email'))
    // console.log(formData.get('password'));
    // console.log(formData);

    const form = document.getElementById('myForm');
    formData = new FormData(form);
    var data = {};

    for (let [key, prop] of formData){
        data[key] = prop;
    }

    data = JSON.stringify(data, null, 2);
    console.log(data);

    // axios.post('https://reqres.in/api/register',{
    //     email: 'eve.holt@reqres.in',
    //     password: 'pistol'
    // }).then(result=>{
    //     console.log(result);
    // }).catch(err=>{
    //     console.log(err, err.response);
    // })
    // let axiosConfig = {
    //     headers: {
    //         'Content-Type': 'application/json',
    //     }
    //   };

    axios.post('https://reqres.in/api/register', data
    ).then(result=>{
        console.log(result);
    }).catch(err=>{
        console.log(err, err.response);
    })
}

getBtn.addEventListener('click', getData);
postBtn.addEventListener('click', postData);